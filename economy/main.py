import random
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation


class Agent:
    def __init__(self,
                 x,
                 y,
                 energy,
                 max_age,
                 immunity_prob,
                 infection_prob,
                 transmission_prob):
        self.x = x
        self.y = y
        self.energy = energy
        self.age = 0
        self.max_age = max_age
        self.sick = False
        self.sick_duration = 0
        self.immunity = False
        self.immunity_prob = immunity_prob
        self.infection_prob = infection_prob
        self.transmission_prob = transmission_prob

    def move(self, x, y):
        # TODO Implement energy cost of movement
        self.x = x
        self.y = y

    def metabolize(self, energy):
        self.energy += energy

    def reproduce(self, x, y):
        if self.energy // 2 <= 0:
            return None

        child = Agent(
            x, y,
            self.energy // 2,
            self.max_age, self.immunity_prob,
            self.infection_prob, self.transmission_prob)
        self.energy //= 2
        return child

    def update(self, grid, agents):
        # Find the closest cell with sugar
        min_distance = float('inf')
        target_x, target_y = -1, -1
        for i in range(grid.shape[0]):
            for j in range(grid.shape[1]):
                if grid[i, j] > 0:
                    distance = np.sqrt((i - self.x) ** 2 + (j - self.y) ** 2)
                    if distance < min_distance:
                        min_distance = distance
                        target_x, target_y = i, j

        # Move and metabolize
        if target_x != -1 and target_y != -1:
            self.move(target_x, target_y)

        self.metabolize(grid[target_x, target_y])
        grid[target_x, target_y] = 0

        # Age and check for death
        self.age += 1
        if self.age > self.max_age or self.energy <= 0:
            return None

        # Update sick status and check for death
        if self.sick:
            self.sick_duration += 1
            if random.random() < self.immunity_prob:
                self.sick = False
                self.immunity = True
            elif self.sick_duration > 10:  # Arbitrary sickness duration
                return None

        # Check for infection
        if not self.immunity:
            if random.random() < self.infection_prob:
                self.sick = True
            else:
                for agent in agents:
                    if agent.sick and agent.x == self.x and agent.y == self.y:
                        if random.random() < self.transmission_prob:
                            self.sick = True
                            break

        return self


def generate_grid(size, max_resource):
    return np.random.randint(0, max_resource + 1, size=(size, size))


def run_simulation_no_vis(grid_size=51, max_resource=10, num_agents=100, max_age=100, immunity_prob=0.1, infection_prob=0.05, transmission_prob=0.2):
    grid = generate_grid(grid_size, max_resource)
    agents = [Agent(
        x=random.randint(0, grid_size - 1),
        y=random.randint(0, grid_size - 1),
        energy=random.randint(1, max_resource),
        max_age=max_age,
        immunity_prob=immunity_prob,
        infection_prob=infection_prob,
        transmission_prob=transmission_prob) for _ in range(num_agents)]

    time_steps = 100
    for t in range(time_steps):
        new_agents = []
        for agent in agents:
            updated_agent = agent.update(grid, agents)
            if updated_agent is not None:
                new_agents.append(updated_agent)
        agents = new_agents

        # Add reproduction step
        for agent in agents:
            if random.random() < 0.05:  # Arbitrary reproduction probability
                child_x = random.randint(0, grid_size - 1)
                child_y = random.randint(0, grid_size - 1)
                child = agent.reproduce(child_x, child_y)
                if child is not None:
                    agents.append(child)


global savecounter
savecounter = 0

def visualize_system(grid, agents, ax):
    ax.clear()
    ax.set_title('Sugarscape Simulation')

    # Plot the grid with sugar resources
    im = ax.imshow(grid, cmap='YlOrRd', vmin=0, vmax=np.max(grid))

    # Plot the agents
    healthy_x, healthy_y, sick_x, sick_y = [], [], [], []
    for agent in agents:
        if agent.sick:
            sick_x.append(agent.x)
            sick_y.append(agent.y)
        else:
            healthy_x.append(agent.x)
            healthy_y.append(agent.y)

    ax.scatter(healthy_x, healthy_y, c='b', label='Healthy')
    ax.scatter(sick_x, sick_y, c='r', label='Sick')
    ax.legend()

    global savecounter
    plt.savefig(f"images/test{savecounter}.png")
    savecounter += 1

    return im


def update(t, system):
    grid, agents, ax = system.grid, system.agents, system.ax
    new_agents = []
    for agent in agents:
        updated_agent = agent.update(grid, agents)
        if updated_agent:
            new_agents.append(updated_agent)
    agents = new_agents

    # Add reproduction step
    children = []
    for agent in agents:
        if random.random() < 0.05:  # Arbitrary reproduction probability
            child_x, child_y = random.randint(0, grid.shape[0] - 1), random.randint(0, grid.shape[1] - 1)
            child = agent.reproduce(child_x, child_y)
            if child is not None:
                children.append(child)

    system.agents = agents + children

    return [visualize_system(grid, agents, ax)]


class System:
    def __init__(self, grid, agents, ax):
        self.grid = grid
        self.agents = agents
        self.ax = ax


def run_simulation(grid_size=51,
                   max_resource=10,
                   num_agents=100,
                   max_age=100,
                   immunity_prob=0.1,
                   infection_prob=0.05,
                   transmission_prob=0.2):
    grid = generate_grid(grid_size, max_resource)
    agents = [Agent(random.randint(0,
                                   grid_size - 1),
                    random.randint(0,
                                   grid_size - 1),
                    random.randint(1,
                                   max_resource),
                    max_age,
                    immunity_prob,
                    infection_prob,
                    transmission_prob) for _ in range(num_agents)]


    fig, ax = plt.subplots(figsize=(8, 8))

    system = System(grid, agents, ax)

    _ = animation.FuncAnimation(fig,
                                update,
                                frames=100,
                                fargs=(system,),
                                blit=False,
                                interval=200,
                                repeat=False)

    plt.show()


if __name__ == "__main__":
    run_simulation()
