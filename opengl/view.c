void cross(float* a, float* b, float* result) {
    result[0] = a[1] * b[2] - a[2] * b[1];
    result[1] = a[2] * b[0] - a[0] * b[2];
    result[2] = a[0] * b[1] - a[1] * b[0];
}

void normalize(float* v) {
    float length = sqrtf(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);
    v[0] /= length;
    v[1] /= length;
    v[2] /= length;
}

float dot(float* a, float* b) {
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}

void lookAt(float* position, float* target, float* up, float* viewMatrix) {
    float zAxis[3];
    zAxis[0] = position[0] - target[0];
    zAxis[1] = position[1] - target[1];
    zAxis[2] = position[2] - target[2];
    normalize(zAxis);

    float xAxis[3];
    cross(up, zAxis, xAxis);
    normalize(xAxis);

    float yAxis[3];
    cross(zAxis, xAxis, yAxis);

    viewMatrix[0] = xAxis[0];
    viewMatrix[1] = yAxis[0];
    viewMatrix[2] = zAxis[0];
    viewMatrix[3] = 0.0f;

    viewMatrix[4] = xAxis[1];
    viewMatrix[5] = yAxis[1];
    viewMatrix[6] = zAxis[1];
    viewMatrix[7] = 0.0f;

    viewMatrix[8] = xAxis[2];
    viewMatrix[9] = yAxis[2];
    viewMatrix[10] = zAxis[2];
    viewMatrix[11] = 0.0f;

    viewMatrix[12] = -dot(xAxis, position);
    viewMatrix[13] = -dot(yAxis, position);
    viewMatrix[14] = -dot(zAxis, position);
    viewMatrix[15] = 1.0f;
}
