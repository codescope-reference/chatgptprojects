#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void framebuffer_size_callback(GLFWwindow *window, int width, int height);

#define check(code)                                                            \
  {                                                                            \
    if (code != 0) {                                                           \
      exit(1);                                                                 \
    }                                                                          \
  }

unsigned int createShaderProgram(const char *vertexShaderPath,
                                 const char *fragmentShaderPath) {
  // Read shader source files
  FILE *vertexShaderFile = fopen(vertexShaderPath, "rb");
  fseek(vertexShaderFile, 0, SEEK_END);
  long vertexShaderSize = ftell(vertexShaderFile);
  fseek(vertexShaderFile, 0, SEEK_SET);
  char *vertexShaderSource = malloc(vertexShaderSize + 1);
  fread(vertexShaderSource, 1, vertexShaderSize, vertexShaderFile);
  fclose(vertexShaderFile);
  vertexShaderSource[vertexShaderSize] = 0;

  FILE *fragmentShaderFile = fopen(fragmentShaderPath, "rb");
  fseek(fragmentShaderFile, 0, SEEK_END);
  long fragmentShaderSize = ftell(fragmentShaderFile);
  fseek(fragmentShaderFile, 0, SEEK_SET);
  char *fragmentShaderSource = malloc(fragmentShaderSize + 1);
  fread(fragmentShaderSource, 1, fragmentShaderSize, fragmentShaderFile);
  fclose(fragmentShaderFile);
  fragmentShaderSource[fragmentShaderSize] = 0;

  // Compile shaders
  unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
  glCompileShader(vertexShader);

  unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
  glCompileShader(fragmentShader);

  // Link shaders into a program
  unsigned int shaderProgram = glCreateProgram();
  glAttachShader(shaderProgram, vertexShader);
  glAttachShader(shaderProgram, fragmentShader);
  glLinkProgram(shaderProgram);

  // Clean up
  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);
  free(vertexShaderSource);
  free(fragmentShaderSource);

  return shaderProgram;
}

// What is the program doing?
// Deciding window settings
// Creating a window
// Loading OpenGL function pointers
// Setting a viewport and resize callback
// Creating a shader program by loading the shaders, compiling and linking them
// Then we're setting up vertex data. It's possible to have multiple sets of vertex data
//   and quickly switch between them. All this vertex data is encapsulated in a Vertex Array Object (VAO).
//   The actual data is located inside one or more Vertex Buffer Objects (VBO).
//   You have to tell OpenGL how the data referred to in the shaders (in arguments) is laid out in the VBO.
//   This happens in the glVertexAttribPointer functions. In glEnableVertexAttribArray(id) you tell OpenGL 
//   that you want to use the data in the VBO for the attribute with that id you specified in glVertexAttribPointer.
// The concept of "binding" and "unbinding" things is roughly equivalent to making them active or inactive.
//
int main(void) {
  // Example vertex data for a colored triangle
  float vertices[] = {// positions       // colors
                      -0.5f, -0.5f, 0.0f,    1.0f, 0.0f, 0.0f,
                      0.5f, -0.5f, 0.0f,     0.0f, 1.0f, 0.0f,
                      0.0f, 0.5f, 0.0f,      0.0f, 0.0f,  1.0f};

  // Initialize GLFW
  if (!glfwInit()) {
    fprintf(stderr, "Failed to initialize GLFW\n");
    return -1;
  }

  // Create a windowed mode window and its OpenGL context
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  GLFWwindow *window = glfwCreateWindow(800, 600, "LearnOpenGL", NULL, NULL);
  if (window == NULL) {
    fprintf(stderr, "Failed to create GLFW window\n");
    glfwTerminate();
    return -1;
  }

  // Make the window's context current
  glfwMakeContextCurrent(window);

  // Load OpenGL function pointers using GLAD
  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
    fprintf(stderr, "Failed to initialize GLAD\n");
    return -1;
  }

  // Set the viewport
  glViewport(0, 0, 800, 600);
  glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

  // Create shader program
  unsigned int shaderProgram =
      createShaderProgram("vertex_shader.glsl", "fragment_shader.glsl");

  // Generate VBO and VAO and set up their attributes
  unsigned int VBO, VAO;
  glGenBuffers(1, &VBO);
  glGenVertexArrays(1, &VAO);

  glBindVertexArray(VAO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_DYNAMIC_DRAW);

  // Set up vertex attribute pointers and enable them
  // Position attribute
  // What is the meaning of the input arguments?
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void *)0);
  glEnableVertexAttribArray(0);

  // Color attribute
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
                        (void *)(3 * sizeof(float)));
  glEnableVertexAttribArray(1);

  // Why do we unbind?
  glBindVertexArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // Main loop
  while (!glfwWindowShouldClose(window)) {
    // Process input
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
      glfwSetWindowShouldClose(window, 1);

    float currentTime = (float)glfwGetTime();
    GLint timeLocation = glGetUniformLocation(shaderProgram, "time");
    glUseProgram(shaderProgram);
    glUniform1f(timeLocation, currentTime);

    // float mixFactor = 0.5f;
    // GLint mixLocation = glGetUniformLocation(shaderProgram, "mixFactor");
    // glUniform1f(mixLocation, mixFactor);

    float colorFactor = (sin(currentTime) / 2.0f) + 0.5f;
    vertices[3] = colorFactor;
    vertices[9] = colorFactor;

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    // glBufferSubData is used to update the data in the buffer without allocating new memory on the GPU
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    // Render
    glClearColor(0.8f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // Set up vertex buffers

    // Use program
    glBindVertexArray(VAO);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glBindVertexArray(0);

    // Issue draw commands
    // ??

    // Swap buffers and poll for events
    glfwSwapBuffers(window);
    glfwPollEvents();
  }

  // Clean up
  glfwTerminate();
  return 0;
}

void framebuffer_size_callback(GLFWwindow *window, int width, int height) {
  (void *)window;
  glViewport(0, 0, width, height);
}
