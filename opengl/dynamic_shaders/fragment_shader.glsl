// Set the GLSL version to 4.60
#version 460 core

// Declare the input variable to receive the color from the vertex shader
in vec3 ourColor;
// Declare the output variable for the final fragment color
out vec4 FragColor;

// uniform float mixFactor;

// The main function of the fragment shader
void main() {
    // Set the output fragment color using the received color and an alpha value of 1.0
    // vec3 resultColor = mix(vec3(1.0, 1.0, 1.0), ourColor, mixFactor);
    // vec3 resultColor = step(mixFactor, ourColor);
    // FragColor = vec4(resultColor, 1.0);
    FragColor = vec4(ourColor, 1.0);
}

