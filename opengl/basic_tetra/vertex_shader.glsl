// Set the GLSL version to 4.60
#version 460 core

// Declare the input vertex attribute: position (location = 0)
layout (location = 0) in vec3 aPos;
// Declare the input vertex attribute: color (location = 1)
layout (location = 1) in vec3 aColor;

// Declare an output variable to pass the color to the fragment shader
out vec3 ourColor;


uniform mat3 transform;
uniform mat4 projection;


// The main function of the vertex shader
void main() {
    // Set the output vertex position
    gl_Position = projection * vec4(transform * aPos, 1.0);
    // Pass the input color to the output variable for the fragment shader
    ourColor = aColor;
}

