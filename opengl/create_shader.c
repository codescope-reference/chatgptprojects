unsigned int createShaderProgram(const char *vertexShaderPath,
                                 const char *fragmentShaderPath) {
  // Read shader source files
  FILE *vertexShaderFile = fopen(vertexShaderPath, "rb");
  fseek(vertexShaderFile, 0, SEEK_END);
  long vertexShaderSize = ftell(vertexShaderFile);
  fseek(vertexShaderFile, 0, SEEK_SET);
  char *vertexShaderSource = malloc(vertexShaderSize + 1);
  fread(vertexShaderSource, 1, vertexShaderSize, vertexShaderFile);
  fclose(vertexShaderFile);
  vertexShaderSource[vertexShaderSize] = 0;

  FILE *fragmentShaderFile = fopen(fragmentShaderPath, "rb");
  fseek(fragmentShaderFile, 0, SEEK_END);
  long fragmentShaderSize = ftell(fragmentShaderFile);
  fseek(fragmentShaderFile, 0, SEEK_SET);
  char *fragmentShaderSource = malloc(fragmentShaderSize + 1);
  fread(fragmentShaderSource, 1, fragmentShaderSize, fragmentShaderFile);
  fclose(fragmentShaderFile);
  fragmentShaderSource[fragmentShaderSize] = 0;

  // Compile shaders
  unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
  glCompileShader(vertexShader);

  GLint success;
  glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
  if (!success) {
    char infoLog[512];
    glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);

    printf("ERROR::SHADER::VERTEX::COMPILATION_FAILED\n");

    printf("Shader compilation failed: %s\n", infoLog);
  }

  unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
  glCompileShader(fragmentShader);

  glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
  if (!success) {
    char infoLog[512];
    glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
    printf("ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n");
  }

  // Link shaders into a program
  unsigned int shaderProgram = glCreateProgram();
  glAttachShader(shaderProgram, vertexShader);
  glAttachShader(shaderProgram, fragmentShader);
  glLinkProgram(shaderProgram);

  glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
  if (!success) {
    char infoLog[512];
    glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
    printf("ERROR::SHADER::PROGRAM::LINKING_FAILED\n");

    printf("Shader linking failed: %s\n", infoLog);
  }

  // Clean up
  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);
  free(vertexShaderSource);
  free(fragmentShaderSource);

  return shaderProgram;
}
