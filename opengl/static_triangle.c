#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void framebuffer_size_callback(GLFWwindow *window, int width, int height);

#define check(code)                                                            \
  {                                                                            \
    if (code != 0) {                                                           \
      exit(1);                                                                 \
    }                                                                          \
  }

unsigned int createShaderProgram(const char *vertexShaderPath,
                                 const char *fragmentShaderPath) {
  // Read shader source files
  FILE *vertexShaderFile = fopen(vertexShaderPath, "rb");
  fseek(vertexShaderFile, 0, SEEK_END);
  long vertexShaderSize = ftell(vertexShaderFile);
  fseek(vertexShaderFile, 0, SEEK_SET);
  char *vertexShaderSource = malloc(vertexShaderSize + 1);
  fread(vertexShaderSource, 1, vertexShaderSize, vertexShaderFile);
  fclose(vertexShaderFile);
  vertexShaderSource[vertexShaderSize] = 0;

  FILE *fragmentShaderFile = fopen(fragmentShaderPath, "rb");
  fseek(fragmentShaderFile, 0, SEEK_END);
  long fragmentShaderSize = ftell(fragmentShaderFile);
  fseek(fragmentShaderFile, 0, SEEK_SET);
  char *fragmentShaderSource = malloc(fragmentShaderSize + 1);
  fread(fragmentShaderSource, 1, fragmentShaderSize, fragmentShaderFile);
  fclose(fragmentShaderFile);
  fragmentShaderSource[fragmentShaderSize] = 0;

  // Compile shaders
  unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
  glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
  glCompileShader(vertexShader);

  unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
  glCompileShader(fragmentShader);

  // Link shaders into a program
  unsigned int shaderProgram = glCreateProgram();
  glAttachShader(shaderProgram, vertexShader);
  glAttachShader(shaderProgram, fragmentShader);
  glLinkProgram(shaderProgram);

  // Clean up
  glDeleteShader(vertexShader);
  glDeleteShader(fragmentShader);
  free(vertexShaderSource);
  free(fragmentShaderSource);

  return shaderProgram;
}

int main(void) {
  // Example vertex data for a colored triangle
  float vertices[] = {// positions       // colors
                      -0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 0.5f, -0.5f, 0.0f,
                      0.0f,  1.0f,  0.0f, 0.0f, 0.5f, 0.0f, 0.0f, 0.0f,  1.0f};

  // Initialize GLFW
  if (!glfwInit()) {
    fprintf(stderr, "Failed to initialize GLFW\n");
    return -1;
  }

  // Create a windowed mode window and its OpenGL context
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  GLFWwindow *window = glfwCreateWindow(800, 600, "LearnOpenGL", NULL, NULL);
  if (window == NULL) {
    fprintf(stderr, "Failed to create GLFW window\n");
    glfwTerminate();
    return -1;
  }

  // Make the window's context current
  glfwMakeContextCurrent(window);

  // Load OpenGL function pointers using GLAD
  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
    fprintf(stderr, "Failed to initialize GLAD\n");
    return -1;
  }

  // Set the viewport
  glViewport(0, 0, 800, 600);
  glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

  // Create shader program
  unsigned int shaderProgram =
      createShaderProgram("vertex_shader.glsl", "fragment_shader.glsl");

  // Generate VBO and VAO and set up their attributes
  unsigned int VBO, VAO;
  glGenBuffers(1, &VBO);
  glGenVertexArrays(1, &VAO);

  glBindVertexArray(VAO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  // Set up vertex attribute pointers and enable them
  // Position attribute
  // What is the meaning of the input arguments?
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void *)0);
  glEnableVertexAttribArray(0);

  // Color attribute
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
                        (void *)(3 * sizeof(float)));
  glEnableVertexAttribArray(1);

  // Why do we unbind?
  glBindVertexArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  // Main loop
  while (!glfwWindowShouldClose(window)) {
    // Process input
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
      glfwSetWindowShouldClose(window, 1);

    // Render
    glClearColor(0.8f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // Set up vertex buffers
    // ??

    // Use program
    glBindVertexArray(VAO);
    glUseProgram(shaderProgram);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glBindVertexArray(0);

    // Issue draw commands
    // ??

    // Swap buffers and poll for events
    glfwSwapBuffers(window);
    glfwPollEvents();
  }

  // Clean up
  glfwTerminate();
  return 0;
}

void framebuffer_size_callback(GLFWwindow *window, int width, int height) {
  (void *)window;
  glViewport(0, 0, width, height);
}
