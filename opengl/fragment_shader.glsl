// // Set the GLSL version to 4.60
// #version 330 core
// 
// // Declare the input variable to receive the color from the vertex shader
// in vec3 vertexColor;
// in vec3 vertexNormal;
// in vec3 vertexPosition;
// 
// // Declare the output variable for the final fragment color
// out vec4 FragColor;
// 
// uniform vec3 lightPosition;
// uniform vec3 viewPosition;
// 
// 
// // The main function of the fragment shader
// void main() {
//     vec3 ambientColor = 0.2 * vertexColor;
// 
//     vec3 lightDirection = normalize(lightPosition - vertexPosition);
//     float diffuseIntensity = max(dot(vertexNormal, lightDirection), 0.0);
//     vec3 diffuseColor = diffuseIntensity * vertexColor;
// 
//     vec3 viewDirection = normalize(viewPosition - vertexPosition);
//     vec3 halfwayDirection = normalize(lightDirection + viewDirection);
//     float specularIntensity = pow(max(dot(vertexNormal, halfwayDirection), 0.0), 32.0);
//     vec3 specularColor = specularIntensity * vec3(1.0, 1.0, 1.0);
// 
//     vec3 finalColor = ambientColor + diffuseColor + specularColor;
// 
//     // Set the output fragment color using the received color and an alpha value of 1.0
//     // FragColor = vec4(vertexColor, 1.0);
//     FragColor = vec4(finalColor, 1.0);
//     // FragColor = vec4(vertexNormal * 0.5 + 0.5, 1.0);
// }

#version 460 core

in vec3 vertexColor;
in vec3 vertexNormal;

out vec4 FragColor;

uniform vec3 lightPos;
uniform vec3 lightColor;
uniform vec3 viewPos;

void main() {
    // Normalize the vertex normal
    vec3 N = normalize(vertexNormal);

    // Calculate the light direction
    vec3 lightDir = normalize(lightPos - vec3(gl_FragCoord));

    // Calculate the diffuse component
    float diff = max(dot(N, lightDir), 0.0);

    // Calculate the view direction
    vec3 viewDir = normalize(viewPos - vec3(gl_FragCoord));

    // Calculate the reflection direction
    vec3 reflectDir = reflect(-lightDir, N);

    // Calculate the specular component
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);

    // Calculate the final color
    vec3 ambient = 0.1 * vertexColor;
    vec3 diffuse = diff * vertexColor;
    vec3 specular = spec * lightColor;

    vec3 finalColor = ambient + diffuse + specular;

    FragColor = vec4(finalColor, 1.0);
}

