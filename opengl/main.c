#include "includes.h"

void framebuffer_size_callback(GLFWwindow *window, int width, int height);

float projectionMatrix[16];

#define WIDTH 800
#define HEIGHT 800

// What is the program doing?
// Deciding window settings
// Creating a window
// Loading OpenGL function pointers
// Setting a viewport and resize callback
// Creating a shader program by loading the shaders, compiling and linking them
// Then we're setting up vertex data. It's possible to have multiple sets of
// vertex data
//   and quickly switch between them. All this vertex data is encapsulated in a
//   Vertex Array Object (VAO). The actual data is located inside one or more
//   Vertex Buffer Objects (VBO). You have to tell OpenGL how the data referred
//   to in the shaders (in arguments) is laid out in the VBO. This happens in
//   the glVertexAttribPointer functions. In glEnableVertexAttribArray(id) you
//   tell OpenGL that you want to use the data in the VBO for the attribute with
//   that id you specified in glVertexAttribPointer.
// The concept of "binding" and "unbinding" things is roughly equivalent to
// making them active or inactive.
//
int main(void) {

  float vertices[] = {
      // Position          // Color           // Normal
      0.0f,  1.0f,  0.0f,  1.0f, 0.0f, 0.0f, 0.0f,  1.0f,  0.0f,  // Vertex 1
      1.0f,  -1.0f, 1.0f,  0.0f, 1.0f, 0.0f, 1.0f,  -1.0f, 1.0f,  // Vertex 2
      -1.0f, -1.0f, 1.0f,  0.0f, 0.0f, 1.0f, -1.0f, -1.0f, 1.0f,  // Vertex 3
      -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 0.0f, -1.0f, -1.0f, -1.0f, // Vertex 4
  };
  GLuint indices[] = {
      0, 1, 2, // Triangle 1
      0, 2, 3, // Triangle 2
      0, 3, 1, // Triangle 3
      1, 3, 2  // Triangle 4
  };

  // Initialize GLFW
  if (!glfwInit()) {
    fprintf(stderr, "Failed to initialize GLFW\n");
    return -1;
  }

  // Create a windowed mode window and its OpenGL context
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  GLFWwindow *window =
      glfwCreateWindow(WIDTH, HEIGHT, "LearnOpenGL", NULL, NULL);
  if (window == NULL) {
    fprintf(stderr, "Failed to create GLFW window\n");
    glfwTerminate();
    return -1;
  }

  // Make the window's context current
  glfwMakeContextCurrent(window);

  // Load OpenGL function pointers using GLAD
  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
    fprintf(stderr, "Failed to initialize GLAD\n");
    return -1;
  }

  // Set the viewport
  glViewport(0, 0, WIDTH, HEIGHT);
  glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

  // Call the callback function once to initialize the projection matrix
  int windowWidth, windowHeight;
  glfwGetFramebufferSize(window, &windowWidth, &windowHeight);
  framebuffer_size_callback(window, windowWidth, windowHeight);

  // Create shader program
  unsigned int shaderProgram =
      createShaderProgram("vertex_shader.glsl", "fragment_shader.glsl");

  // Generate VBO and VAO and set up their attributes
  unsigned int VBO, VAO;
  glGenBuffers(1, &VBO);
  glGenVertexArrays(1, &VAO);

  glBindVertexArray(VAO);
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  // Set up vertex attribute pointers and enable them
  // Position attribute
  // What is the meaning of the input arguments?
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float), (void *)0);
  glEnableVertexAttribArray(0);

  // Color attribute
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float),
                        (void *)(3 * sizeof(float)));
  glEnableVertexAttribArray(1);

  glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 9 * sizeof(float),
                        (void *)(6 * sizeof(float)));
  glEnableVertexAttribArray(2);

  // Why do we unbind?
  glBindVertexArray(0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);

  GLuint EBO;
  glGenBuffers(1, &EBO);

  // Make this VAO active
  glBindVertexArray(VAO);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
  glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices,
               GL_STATIC_DRAW);

  glBindVertexArray(0);

  // Main loop
  while (!glfwWindowShouldClose(window)) {
    // Process input
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
      glfwSetWindowShouldClose(window, 1);

    glUseProgram(shaderProgram);
    float currentTime = glfwGetTime();
    GLint transformLocation = glGetUniformLocation(shaderProgram, "model");
    float transform[16] = {
        1.0f, 0.0f, 0.0f, 0.0f,

        0.0f, 1.0f, 0.0f, 0.0f,

        0.0f, 0.0f, 1.0f, 0.0f,

        0.0f, 0.0f, 0.0f, 1.0f,
        // cos(currentTime),
        // 0.0f,
        // sin(currentTime),
        // 0.0f,

        // 0.0f,
        // 1.0f,
        // 0.0f,
        // 0.0f,

        // -sin(currentTime),
        // 0.0f,
        // cos(currentTime),
        // 0.0f,

        // 0.0f,
        // 0.0f,
        // 0.0f,
        // 1.0f,
    };
    glUniformMatrix4fv(transformLocation, 1, GL_FALSE, transform);

    GLint projectionLocation =
        glGetUniformLocation(shaderProgram, "projection");
    glUniformMatrix4fv(projectionLocation, 1, GL_FALSE, projectionMatrix);

    GLint lightPositionLocation =
        glGetUniformLocation(shaderProgram, "lightPosition");
    GLint viewPositionLocation =
        glGetUniformLocation(shaderProgram, "viewPosition");
    GLint viewLocation = glGetUniformLocation(shaderProgram, "view");

    float lightPosition[3] = {5.0f, 5.0f, 5.0f};
    float viewPosition[3] = {0.0f, 0.0f, 3.0f};
    glUniform3fv(lightPositionLocation, 1, lightPosition);
    glUniform3fv(viewPositionLocation, 1, viewPosition);


    // Set up the viewMatrix
    // float viewMatrix[16] = {
    //     1.0f, 0.0f, 0.0f, 0.0f,
    //     0.0f, 1.0f, 0.0f, 0.0f,
    //     0.0f, 0.0f, 1.0f, 0.0f,
    //     -viewPosition[0], -viewPosition[1], -viewPosition[2], 1.0f,
    // };

    float cameraTarget[3] = {0.0f, 0.0f, 0.0f};
    float cameraUp[3] = {0.0f, 1.0f, 0.0f};

    float viewMatrix[16];
    lookAt(viewPosition, cameraTarget, cameraUp, viewMatrix);
    glUniformMatrix4fv(viewLocation, 1, GL_FALSE, viewMatrix);

    // Render
    glClearColor(0.8f, 0.3f, 0.3f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    // Set up vertex buffers

    // Use program
    glBindVertexArray(VAO);
    glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, 0);
    glBindVertexArray(0);

    // Issue draw commands
    // ??

    // Swap buffers and poll for events
    glfwSwapBuffers(window);
    glfwPollEvents();
  }

  // Clean up
  glfwTerminate();
  return 0;
}

void framebuffer_size_callback(GLFWwindow *window, int width, int height) {
  float aspectRatio = (float)width / (float)height;
  float size = 2.0f; // Adjust this value to control the zoom level
  float left = -size;
  float right = size;
  float bottom = -size / aspectRatio;
  float top = size / aspectRatio;
  float near = -1.0f;
  float far = 1.0f;

  ortho(left, right, bottom, top, near, far, projectionMatrix);

  glViewport(0, 0, width, height);
}

// void framebuffer_size_callback(GLFWwindow *window, int width, int height) {
//   (void *)window;
//   glViewport(0, 0, width, height);
// }
