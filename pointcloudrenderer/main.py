import pyvista as pv
from pyvista import examples
import numpy as np
import matplotlib.pyplot as plt

# Load model
# model = examples.download_st_helens()
# warped = model.warp_by_scalar("Elevation")
# points = warped.points
# model = examples.download_owl()
# points = model.points[::100]

model = examples.download_bunny()
points = model.points

# # Generate random points
# n_points = 1000
# # points = np.random.rand(n_points, 3) * 100
# phis = np.random.rand(n_points) * 2 * np.pi
# thetas = np.random.rand(n_points) * np.pi
# radius = 10
# points = (radius * np.array([np.sin(thetas) * np.cos(phis),
#                             np.sin(thetas) * np.sin(phis),
#                             np.cos(thetas)])).T

# Create a point cloud using pyvista
point_cloud = pv.PolyData(points)


z_values = points[:, 2]
normalized_z = (z_values - np.min(z_values)) \
    / (np.max(z_values) - np.min(z_values))

# Create a color map
cmap = plt.cm.get_cmap("viridis")
colors = cmap(normalized_z)[:, :3] # Convert RGBA to RGB

# Add the colors to the point point_cloud
point_cloud["colors"] = colors


# Create a plotter and add the point cloud to it
plotter = pv.Plotter()
plotter.add_mesh(point_cloud,
                 render_points_as_spheres=True,
                 point_size=4,
                 scalars="colors")

# Display the point cloud
plotter.show()
