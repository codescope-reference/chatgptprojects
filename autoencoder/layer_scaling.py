def down(x, k, p, s):
    return (x - k + 2 * p) // s + 1


def up(x, k, p, s, op):
    return (x - 1) * s - 2 * p + k + op


layers = [(3, 1, 2), (3, 1, 2), (3, 1, 2)]
layers2 = [(3, 1, 2), (4, 1, 4), (5, 1, 4)]


def apply_layers(x, layers):
    print(x)
    for k, p, s in layers:
        x = down(x, k, p, s)
        print(x)

    for i, (k, p, s) in enumerate(reversed(layers)):
        x = up(x, k, p, s, op=0)
        if i == 0:
            x += 1
        if i == 1:
            x += 2
        print(x)

apply_layers(256, layers)
print("--------")
# apply_layers(300, layers)
apply_layers(256, layers2)
