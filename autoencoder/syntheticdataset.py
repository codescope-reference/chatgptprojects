from torch.utils.data import Dataset
from PIL import Image
import os


class SyntheticDataset(Dataset):
    def __init__(self, root_dir, transform=None):
        self.root_dir = root_dir
        self.transform = transform
        self.file_list = [f for f in os.listdir(root_dir) if f.endswith('.png')]

    def __len__(self):
        return len(self.file_list)

    def __getitem__(self, idx):
        img_name = os.path.join(self.root_dir, self.file_list[idx])
        img = Image.open(img_name) # .convert('L')
        if self.transform:
            img = self.transform(img)
        return img
