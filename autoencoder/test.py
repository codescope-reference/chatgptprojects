import torch
from autoencoder import Autoencoder
from PIL import Image
import torchvision.transforms as transforms


def tensor_to_image(tensor):
    img = tensor.cpu().clone().detach().numpy()
    img = img.transpose(1, 2, 0)
    img = (img * 255).clip(0, 255).astype('uint8')
    return Image.fromarray(img)


# Load the saved model
model_path = 'autoencoder.pth'
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
loaded_model = Autoencoder().to(device)
loaded_model.load_state_dict(torch.load(model_path))
loaded_model.eval()

# Preprocess the test image
test_image_path = "training_data_square/synthetic_0.png"
output_image_path = 'output_image.png'
transform = transforms.Compose([transforms.Resize((256, 256)), transforms.ToTensor()])
test_image = Image.open(test_image_path)
test_image_tensor = transform(test_image).unsqueeze(0).to(device)

# Pass the test image through the autoencoder
output_tensor = loaded_model(test_image_tensor)

# Convert the output tensor to a PIL
output_image = tensor_to_image(output_tensor[0])

# Save the output Image
output_image.save(output_image_path)
