import torch.nn as nn


class Autoencoder(nn.Module):
    def __init__(self):
        super(Autoencoder, self).__init__()
        self.encoder = nn.Sequential(
            nn.Conv2d(3,
                      16,
                      kernel_size=3,
                      stride=2,
                      padding=1),
            nn.ReLU(),
            nn.Conv2d(16,
                      32,
                      kernel_size=4,
                      stride=4,
                      padding=1),
            nn.ReLU(),
            nn.Conv2d(32,
                      64,
                      kernel_size=5,
                      stride=4,
                      padding=1),
            nn.ReLU(),

        )
        self.decoder = nn.Sequential(
            nn.ConvTranspose2d(64,
                               32,
                               kernel_size=3,
                               stride=2,
                               padding=1,
                               output_padding=1,
                               ),

            nn.ReLU(),
            nn.ConvTranspose2d(32,
                               16,
                               kernel_size=4,
                               stride=4,
                               padding=1,
                               output_padding=2,
                               ),
            # 64 ->
            nn.ReLU(),
            nn.ConvTranspose2d(16,
                               3,
                               kernel_size=5,
                               stride=4,
                               padding=1,
                               output_padding=1,
                               ),
            nn.Sigmoid(),
        )

    def forward(self, x):
        # assert tuple(x.shape) == (16, 3, 300, 300)

        x = self.encoder(x)
        # assert tuple(x.shape) == (16, 64, 38, 38), tuple(x.shape)

        x = self.decoder(x)
        # assert tuple(x.shape) == (16, 3, 300, 300), tuple(x.shape)

        return x
