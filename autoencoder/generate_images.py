# Generate synthetic data
# The synthetic is an array of circles
# with some random noise added to their positions

import numpy as np
from PIL import Image, ImageDraw


def generate_synthetic_image(width, height, grid_size, offset_range, fill="#5555ff",
                             square_prob=0.0):
    img = Image.new("RGB", (width, height), color="#333333")
    draw = ImageDraw.Draw(img)

    grid_width = width // grid_size
    grid_height = height // grid_size
    circle_radius = min(grid_width, grid_height) // 4

    for i in range(grid_size):
        for j in range(grid_size):
            x_offset = np.random.randint(-offset_range, offset_range)
            y_offset = np.random.randint(-offset_range, offset_range)

            center_x = (i * grid_width) + grid_width // 2 + x_offset
            center_y = (j * grid_height) + grid_height // 2 + y_offset

            if np.random.random() < square_prob:
                draw.rectangle((center_x - circle_radius, center_y - circle_radius,
                                center_x + circle_radius, center_y + circle_radius),
                               fill=fill)
            else:
                draw.ellipse((center_x - circle_radius, center_y - circle_radius,
                              center_x + circle_radius, center_y + circle_radius),
                             fill=fill)

    return img


def generate_training_images(number_of_images, folder, square_prob=0.0):
    for i in range(number_of_images):
        if (i + 1) % 10 == 0:
            print(f"Generating image {i+1}/{number_of_images}   ", end="\r")

        width, height = 300, 300
        grid_size = 9
        offset_range = 5
        image = generate_synthetic_image(width,
                                         height,
                                         grid_size,
                                         offset_range,
                                         square_prob=square_prob)
        image.save(folder + "/synthetic_" + str(i) + ".png")

    print("")


if __name__ == "__main__":
    # Parse arguments:
    # 1. --test which generates a test Image
    # 2. --train which generates training Images
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("--test", action="store_true", help="Generate a test image")
    parser.add_argument("--train", action="store_true", help="Generate training images")
    parser.add_argument("--square", action="store_true", help="Generate images with some circles replaced by squares")

    args = parser.parse_args()

    if args.test:
        width, height = 300, 300
        grid_size = 9
        offset_range = 5
        image = generate_synthetic_image(width, height, grid_size, offset_range)
        image.save("synthetic.png")
    elif args.train:
        number_of_images = 500
        folder = "training_data"
        generate_training_images(number_of_images, folder)
    elif args.square:
        number_of_images = 10
        folder = "training_data_square"
        generate_training_images(number_of_images, folder, square_prob=0.2)
    else:
        # Print usage
        parser.print_help()
