import os
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision.transforms as transforms
from torch.utils.data import DataLoader
from syntheticdataset import SyntheticDataset
from autoencoder import Autoencoder


def train_autoencoder(model, dataloader, criterion, optimizer, device, epochs):
    model.train()
    for epoch in range(epochs):
        running_loss = 0.0
        for images in dataloader:
            images = images.to(device)

            # Zero the parameter gradients
            optimizer.zero_grad()

            # Forward, loss calculation, backward, and optimization
            outputs = model(images)
            loss = criterion(outputs, images)
            loss.backward()
            optimizer.step()

            # Accumulate loss
            running_loss += loss.item()

        # Print average loss for the epoch
        epoch_loss = running_loss / len(dataloader)
        print(f"Epoch [{epoch + 1}/{epochs}], Loss: {epoch_loss:.4f}")


def save_model(path, model):
    torch.save(model.state_dict(), path)


def load_model(path, device):
    if os.path.exists(path):
        model = Autoencoder().to(device)
        model.load_state_dict(torch.load(path))
        model.eval()
    else:
        model = Autoencoder().to(device)
    return model


# Parameters
root_dir = 'training_data'
batch_size = 16
learning_rate = 0.001
epochs = 20

if torch.cuda.is_available():
    print("Using GPU")
else:
    print("Using CPU")

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Create dataset and dataloader
size = (256, 256)
transform = transforms.Compose([transforms.Resize(size), transforms.ToTensor()])
dataset = SyntheticDataset(root_dir, transform)
dataloader = DataLoader(dataset, batch_size=batch_size, shuffle=True)

# Model path
path = 'autoencoder.pth'

# Create the autoencoder model
model = load_model(path, device)

# Set the loss function and optimizer
criterion = nn.MSELoss()
optimizer = optim.Adam(model.parameters(), lr=learning_rate)

# Train the autoencoder
train_autoencoder(model, dataloader, criterion, optimizer, device, epochs)

# Save the model
save_model(path, model)
