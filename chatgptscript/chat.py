import requests
import json
from pathlib import Path
from time import time

api_key = Path("api.key").read_text().strip()
headers = {
    "Authorization": f"Bearer {api_key}",
    "Content-Type": "application/json"
}


def get_available_models():
    response = requests.get('https://api.openai.com/v1/engines', headers=headers)

    if response.status_code == 200:
        response_data = response.json()
        return [engine['id'] for engine in response_data['data']]
    else:
        print(f"Error: {response.status_code}")
        print(response.text)
        return None


def chat_with_chatgpt(prompt, model="gpt-3.5-turbo"):
    system_prompt = "You are ChatGPT, a large language model trained by OpenAI."
    system_prompt += "Your task is to assist with understanding documentation and code."
    system_prompt += "When asked about a function, please describe the types and meanings of the input arguments, as well as any other relevant information."
    data = {
        "model": model,
        "messages": [{"role": "system",
                      "content": system_prompt},
                     {"role": "user", "content": prompt}]
    }

    api_endpoint = "https://api.openai.com/v1/chat/completions"
    t1 = time()
    response = requests.post(api_endpoint,
                             headers=headers,
                             data=json.dumps(data))
    t2 = time()

    if response.status_code == 200:
        response_data = response.json()
        message = response_data["choices"][0]["message"]["content"]
        return message.strip() + "\n\n(Response generated in {:.2f} seconds)".format(t2 - t1)
    else:
        print(f"Error: {response.status_code}")
        print(response.text)
        return None


def set_model(model):
    print(f"Setting model to '{model}'")
    Path("model").write_text(model)


def read_model():
    model = Path("model").read_text().strip()
    print(f"Using model '{model}'")
    return model


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser()
    parser.add_argument("--list-models", action="store_true")
    parser.add_argument("--set-model", type=str)
    args = parser.parse_args()

    if args.set_model:
        set_model(model=args.set_model)
        exit()

    if args.list_models:
        print("Available models:")
        for model in get_available_models():
            print(model)
        exit()
    else:
        model = read_model()
        prompt = input("Enter a prompt: ")
        response = chat_with_chatgpt(prompt, model=model)
        if response is not None:
            print("ChatGPT's response:", response)


# TODO: Build an interactive chatbot, that uses a loop and keeps the history
# TODO: Build an agent, that can search the internet, perform actions
# TODO: Build an agent, that can formulate and perform tasks.
#   Metatask: "Define your next task"
#   Had some kind of loop to perform tasks, define the next one.
#   First task: Find a business niche and build a business strategy.
