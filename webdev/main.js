import { initializeApp } from "firebase/app";
import { config } from "./config.js";
import { getAuth, createUserWithEmailAndPassword } from "firebase/auth";

const app = initializeApp(config);

const auth = getAuth(app);


const signupForm = document.getElementById("signup-form");

function handleSignupFormSubmit(event) {
    event.preventDefault();

    const email = signupForm["email"].value;
    const password = signupForm["password"].value;

    createUserWithEmailAndPassword(auth, email, password)
        .then((_userCredential) => {
            console.log("User successfully created!");
        }).catch((_error) => {
            console.log("Error creating user");
        });
}

signupForm.addEventListener("submit", handleSignupFormSubmit);


